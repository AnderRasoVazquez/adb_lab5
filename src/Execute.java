public class Execute {
    static private final int mode = Data.LOCKING;
    //static final int mode = Data.NONLOCKING;

    public static void main (String[] args){
        Data mainData = new Data(Data.NONLOCKING, Data.NONLOCKING);

        System.out.println("Comenzando inicialización de los threads.");
        ThreadA threadA = new ThreadA(mode);
        ThreadB threadB = new ThreadB(mode);
        ThreadC threadC = new ThreadC(mode);
        ThreadD threadD = new ThreadD(mode);
        ThreadE threadE = new ThreadE(mode);
        ThreadF threadF = new ThreadF(mode);

        mainData.initializeSharedVariables();
        
        
        mainData.showInitialValues();

        System.out.println("Comenzando ejecución de los threads.");
        new Thread(threadA).start();
        new Thread(threadB).start();
        new Thread(threadC).start();
        new Thread(threadD).start();
        new Thread(threadE).start();
        new Thread(threadF).start();

        mainData.showFinalValues();
    }
}
