
import java.sql.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * MySQL connection manager.
 */
@SuppressWarnings("WeakerAccess")
public class Data {
	
	private Connection conn;
	private Statement st;

	// uso de reservas
	static final int NONLOCKING = 0;
	static final int LOCKING = 1;
	// tipo de reservas
	static final int SHARE_LOCKING = LOCKING;
	static final int EXCLUSIVE_LOCKING = 2*LOCKING;
	// número de transacciones a ejecutar por cada hilo
	static final int NUMBER_OF_ITERATIONS = 100;
	// número de hilos
	static final int NUMBER_OF_THREADS = 3;
	
	static final String X = "X";
	static final String Y = "Y";
	static final String Z = "Z";
	static final String T = "T";
	static final String A = "A";
	static final String B = "B";
	static final String C = "C";
	static final String D = "D";
	static final String E = "E";
	static final String F = "F";
	static final String M = "M";
	
	private int SHARE_MODE;
	private int EXCLUSIVE_MODE;
	
	public Data(int myShareMode, int myExclusiveMode) {
		this.SHARE_MODE = myShareMode;
		this.EXCLUSIVE_MODE = myExclusiveMode;
		
		// Load mysql driver
		try {
			Class.forName("org.gjt.mm.mysql.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		try {
			String pc = "Ander";

			String server = "";
			String port = "";
			String user = "";
			String pass = "";

			if (pc.equals("Guzman")){
				System.out.println("Estableciendo conexión con el SGBD de Guzmán.");
				server = "10.109.236.101";  // Server Guzman
				port = "8306";
				user = "lab5";
				pass = "lab5";
			}else if (pc.equals("Ander")){
				System.out.println("Estableciendo conexión con el SGBD de Ander.");
				server = "192.168.56.1";  // Server Ander
				port = "8306";
				user = "prueba";
				pass = "prueba";
			}
			conn = DriverManager.getConnection("jdbc:mysql://" + server + ":" + port, user, pass);
			conn.setAutoCommit(false);
			st = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		
		// seguir en la pag 16
	}

	private void commit() throws SQLException {
        String sql = "COMMIT";
        st.execute(sql);
    }

	private void rollback() {
        try {
            String sql = "ROLLBACK";
            st.execute(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

	private int getValue(int mode, String variable) throws SQLException {
		String modeSQL = "";
		if (mode == SHARE_LOCKING) {
			modeSQL = "LOCK IN SHARE MODE";
		} else if (mode == EXCLUSIVE_LOCKING) {
			modeSQL = "FOR UPDATE";
		} else if (mode == NONLOCKING){
			modeSQL = "";
		}
		String sql = "SELECT value FROM concurrency_control.variables WHERE name='" + variable + "' " + modeSQL;
		ResultSet res = st.executeQuery(sql);
		res.next();
		return res.getInt("value");
	}

	private boolean setValue(int mode, String variable, int value) throws SQLException {
		String sql = "UPDATE concurrency_control.variables SET value=" + value + " WHERE name='" + variable + "'";
		return st.executeUpdate(sql) > 0;
	}

	public void synchronize() throws SQLException {
		int barrierValue;

		increaseBarrier();

		barrierValue = getBarrierValue();
		System.out.println(barrierValue);
		while (barrierValue < NUMBER_OF_THREADS){
			try{
				Thread.sleep(ThreadLocalRandom.current().nextInt(1,11));
				barrierValue = getBarrierValue();
			}catch (InterruptedException e){
				e.printStackTrace();
			}
		}
	}

	public void increaseBarrier() throws SQLException {
		 try {
	            int barrier = getValue(EXCLUSIVE_LOCKING, M);
	            setValue(EXCLUSIVE_LOCKING, M, barrier + 1);
	            commit();
	        } catch (SQLException e) {
	            rollback();
	        }
	}

	public void decreaseBarrier() throws SQLException {
		 try {
	            int barrier = getValue(EXCLUSIVE_LOCKING, M);
	            setValue(EXCLUSIVE_LOCKING, M, barrier - 1);
	            commit();
	        } catch (SQLException e) {
	            rollback();
	        }
	}

	public int getBarrierValue() throws SQLException {
		 try {
	            int m = getValue(SHARE_LOCKING, M);
	            commit();
	            return m;
	        } catch (SQLException e) {
	            rollback();
	            return -1;
	        }
	}

	public void finish() throws SQLException {
		decreaseBarrier();
	}
	
	
	public boolean procedureA(String name, int counter) {
		try{
			int aValue;
			int tValue;
			int yValue;
			int xValue;
			xValue = getValue(EXCLUSIVE_MODE, X);
			xValue = xValue +1;
			setValue(EXCLUSIVE_MODE, X, xValue);
			System.out.println("WRITE(" + name + Integer.toString(counter+1) + "," + X + ","
					+ Integer.toString(xValue - 1) + "," + Integer.toString(xValue) + ")");
			tValue = getValue(EXCLUSIVE_MODE, T);
			aValue = getValue(EXCLUSIVE_MODE, A);
			yValue = getValue(SHARE_MODE, Y);
			tValue = tValue + yValue;
			aValue = aValue + yValue;
			setValue(EXCLUSIVE_MODE, T, tValue);
			setValue(EXCLUSIVE_MODE, A, aValue);
			System.out.println("WRITE(" + name + Integer.toString(counter+1) + "," + T + "," +
					Integer.toString(tValue - yValue) + "," + Integer.toString(tValue) + ")");
			System.out.println("WRITE(" + name + Integer.toString(counter+1) + "," + A + "," +
					Integer.toString(aValue - yValue) + "," + Integer.toString(aValue) + ")");
			System.out.println("END TRANSACTION " + name + Integer.toString(counter+1));
			this.commit();
			return true;
		}catch (Exception e){
			this.rollback();
			e.printStackTrace();
			return false;
		}
	}

	public boolean procedureB(String name, int counter) {
		try{
			int bValue;
			int tValue;
			int yValue;
			int zValue;
			yValue = getValue(EXCLUSIVE_MODE, Y);
			yValue = yValue +1;
			setValue(EXCLUSIVE_MODE, Y, yValue);
			System.out.println("WRITE(" + name + Integer.toString(counter+1) + "," + Y + ","
					+ Integer.toString(yValue - 1) + "," + Integer.toString(yValue) + ")");
			tValue = getValue(EXCLUSIVE_MODE, T);
			bValue = getValue(EXCLUSIVE_MODE, B);
			zValue = getValue(SHARE_MODE, Z);
			tValue = tValue + zValue;
			bValue = bValue + zValue;
			setValue(EXCLUSIVE_MODE, T, tValue);
			setValue(EXCLUSIVE_MODE, B, bValue);
			System.out.println("WRITE(" + name + Integer.toString(counter+1) + "," + T + "," +
					Integer.toString(tValue - zValue) + "," + Integer.toString(tValue) + ")");
			System.out.println("WRITE(" + name + Integer.toString(counter+1) + "," + B + "," +
					Integer.toString(bValue - zValue) + "," + Integer.toString(bValue) + ")");
			System.out.println("END TRANSACTION " + name + Integer.toString(counter+1));
			this.commit();
			return true;
		}catch (Exception e){
			this.rollback();
			e.printStackTrace();
			return false;
		}
	}

	public boolean procedureC(String name, int counter) {
		try{
			int cValue;
			int tValue;
			int xValue;
			int zValue;
			zValue = getValue(EXCLUSIVE_MODE, Z);
			zValue = zValue +1;
			setValue(EXCLUSIVE_MODE, Z, zValue);
			System.out.println("WRITE(" + name + Integer.toString(counter+1) + "," + Z + ","
					+ Integer.toString(zValue - 1) + "," + Integer.toString(zValue) + ")");
			tValue = getValue(EXCLUSIVE_MODE, T);
			cValue = getValue(EXCLUSIVE_MODE, C);
			xValue = getValue(SHARE_MODE, X);
			tValue = tValue + xValue;
			cValue = cValue + xValue;
			setValue(EXCLUSIVE_MODE, T, tValue);
			setValue(EXCLUSIVE_MODE, C, cValue);
			System.out.println("WRITE(" + name + Integer.toString(counter+1) + "," + T + "," +
					Integer.toString(tValue - xValue) + "," + Integer.toString(tValue) + ")");
			System.out.println("WRITE(" + name + Integer.toString(counter+1) + "," + C + "," +
					Integer.toString(cValue - xValue) + "," + Integer.toString(cValue) + ")");
			System.out.println("END TRANSACTION " + name + Integer.toString(counter+1));
			this.commit();
			return true;
		}catch (Exception e){
			this.rollback();
			e.printStackTrace();
			return false;
		}
	}

	public boolean procedureD(String name, int counter) {
		
//		READ(T) 
//		READ(D) 
//		READ(Z) 
//		T = T+Z 
//		D = D+Z 
//		WRITE(T)
//		WRITE(D)
//		READ(X) 
//		X = X-1 
//		WRITE(X)
		
		try{
		int tValue = getValue(EXCLUSIVE_MODE, T);
		int dValue = getValue(EXCLUSIVE_MODE, D);
		int zValue = getValue(SHARE_MODE, Z);
		tValue = tValue + zValue;
		dValue = dValue + zValue;
		setValue(EXCLUSIVE_MODE, T, tValue);
		setValue(EXCLUSIVE_MODE, D, dValue);
		System.out.println("WRITE(" + name + Integer.toString(counter+1) + "," + T + "," +
				Integer.toString(tValue - zValue) + "," + Integer.toString(tValue) + ")");
		System.out.println("WRITE(" + name + Integer.toString(counter+1) + "," + D + "," +
				Integer.toString(dValue - zValue) + "," + Integer.toString(dValue) + ")");
		int xValue = getValue(EXCLUSIVE_MODE, X);
		xValue = xValue - 1;
		setValue(EXCLUSIVE_MODE, X, xValue);
		System.out.println("WRITE(" + name + Integer.toString(counter+1) + "," + X + ","
					+ Integer.toString(xValue + 1) + "," + Integer.toString(xValue) + ")");
		System.out.println("END TRANSACTION " + name + Integer.toString(counter+1));
		this.commit();
			return true;
		}catch (Exception e){
			this.rollback();
			e.printStackTrace();
			return false;
		}
	}

	public boolean procedureE(String name, int counter) {

//		READ(T) 
//		READ(E) 
//		READ(X) 
//		T = T+X 
//		E = E+X 
//		WRITE(T)
//		WRITE(E)
//		READ(Y) 
//		Y = Y-1 
//		WRITE(Y)

		try{
		int tValue = getValue(EXCLUSIVE_MODE, T);
		int eValue = getValue(EXCLUSIVE_MODE, E);
		int xValue = getValue(SHARE_MODE, X);
		tValue = tValue + xValue;
		eValue = eValue + xValue;
		setValue(EXCLUSIVE_MODE, T, tValue);
		setValue(EXCLUSIVE_MODE, E, eValue);
		System.out.println("WRITE(" + name + Integer.toString(counter+1) + "," + T + "," +
				Integer.toString(tValue - xValue) + "," + Integer.toString(tValue) + ")");
		System.out.println("WRITE(" + name + Integer.toString(counter+1) + "," + E + "," +
				Integer.toString(eValue - xValue) + "," + Integer.toString(eValue) + ")");
		int yValue = getValue(EXCLUSIVE_MODE, Y);
		yValue = yValue - 1;
		System.out.println("WRITE(" + name + Integer.toString(counter+1) + "," + Y + ","
					+ Integer.toString(yValue + 1) + "," + Integer.toString(yValue) + ")");
		System.out.println("END TRANSACTION " + name + Integer.toString(counter+1));
		this.commit();
			return true;
		}catch (Exception e){
			this.rollback();
			e.printStackTrace();
			return false;
		}
	}

	public boolean procedureF(String name, int counter) {
//		READ(T)
//		READ(F)
//		READ(Y)
//		T = T+Y
//		F = F+Y
//		WRITE(T)
//		WRITE(F)
//		READ(Z)
//		Z = Z-1
//		WRITE(Z)

		try{
		int tValue = getValue(EXCLUSIVE_MODE, T);
		int fValue = getValue(EXCLUSIVE_MODE, F);
		int yValue = getValue(SHARE_MODE, Y);
		tValue = tValue + yValue;
		fValue = fValue + yValue;
		setValue(EXCLUSIVE_MODE, T, tValue);
		setValue(EXCLUSIVE_MODE, E, fValue);
		System.out.println("WRITE(" + name + Integer.toString(counter+1) + "," + T + "," +
				Integer.toString(tValue - yValue) + "," + Integer.toString(tValue) + ")");
		System.out.println("WRITE(" + name + Integer.toString(counter+1) + "," + F + "," +
				Integer.toString(fValue - yValue) + "," + Integer.toString(fValue) + ")");
		int zValue = getValue(EXCLUSIVE_MODE, Y);
		zValue = zValue - 1;
		System.out.println("WRITE(" + name + Integer.toString(counter+1) + "," + Z + ","
					+ Integer.toString(zValue + 1) + "," + Integer.toString(zValue) + ")");
		System.out.println("END TRANSACTION " + name + Integer.toString(counter+1));
		this.commit();
			return true;
		}catch (Exception e){
			this.rollback();
			e.printStackTrace();
			return false;
		}
	}



	public void showInitialValues() {
		try{
			System.out.println("Initial value of " + X + ": " + Integer.toString(getValue(SHARE_MODE, X)));
			System.out.println("Initial value of " + Y + ": " + Integer.toString(getValue(SHARE_MODE, Y)));
			System.out.println("Initial value of " + Z + ": " + Integer.toString(getValue(SHARE_MODE, Z)));
			System.out.println("Initial value of " + T + ": " + Integer.toString(getValue(SHARE_MODE, T)));
			System.out.println("Initial value of " + A + ": " + Integer.toString(getValue(SHARE_MODE, A)));
			System.out.println("Initial value of " + B + ": " + Integer.toString(getValue(SHARE_MODE, B)));
			System.out.println("Initial value of " + C + ": " + Integer.toString(getValue(SHARE_MODE, C)));
			System.out.println("Initial value of " + D + ": " + Integer.toString(getValue(SHARE_MODE, D)));
			System.out.println("Initial value of " + E + ": " + Integer.toString(getValue(SHARE_MODE, E)));
			System.out.println("Initial value of " + F + ": " + Integer.toString(getValue(SHARE_MODE, F)));
			System.out.println("Initial value of " + M + ": " + Integer.toString(getValue(SHARE_MODE, M)));
		} catch (Exception e){
			e.printStackTrace();
		}
	}

	public void showFinalValues() {
		try{
			int barrierValue;

			barrierValue = getBarrierValue();

			while (barrierValue < 1) {
				try {
					Thread.sleep(ThreadLocalRandom.current().nextInt(1, 11));
					barrierValue = getBarrierValue();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			while (barrierValue > 0) {
				try {
					Thread.sleep(ThreadLocalRandom.current().nextInt(1, 11));
					barrierValue = getBarrierValue();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}catch (SQLException e){
			e.printStackTrace();
			System.exit(1);
		}
		
		try{
			System.out.println("Final value of " + X + ": " + Integer.toString(getValue(SHARE_MODE, X)));
			System.out.println("Final value of " + Y + ": " + Integer.toString(getValue(SHARE_MODE, Y)));
			System.out.println("Final value of " + Z + ": " + Integer.toString(getValue(SHARE_MODE, Z)));
			System.out.println("Final value of " + T + ": " + Integer.toString(getValue(SHARE_MODE, T)));
			System.out.println("Final value of " + A + ": " + Integer.toString(getValue(SHARE_MODE, A)));
			System.out.println("Final value of " + B + ": " + Integer.toString(getValue(SHARE_MODE, B)));
			System.out.println("Final value of " + C + ": " + Integer.toString(getValue(SHARE_MODE, C)));
			System.out.println("Final value of " + D + ": " + Integer.toString(getValue(SHARE_MODE, D)));
			System.out.println("Final value of " + E + ": " + Integer.toString(getValue(SHARE_MODE, E)));
			System.out.println("Final value of " + F + ": " + Integer.toString(getValue(SHARE_MODE, F)));
			
			System.out.println("Expected final value of " + X + ": " + Integer.toString(0));
			System.out.println("Expected final value of " + Y + ": " + Integer.toString(0));
			System.out.println("Expected final value of " + Z + ": " + Integer.toString(0));
			System.out.println("Expected final value of " + T + ": " + Integer.toString(
				getValue(SHARE_MODE, A) +
				getValue(SHARE_MODE, B) +
				getValue(SHARE_MODE, C) +
				getValue(SHARE_MODE, D) +
				getValue(SHARE_MODE, E) +
				getValue(SHARE_MODE, F))
			);

		} catch (Exception e){
			e.printStackTrace();
			System.exit(1);
		}
	}

	public void initializeSharedVariables() {
        try {
            setValue(EXCLUSIVE_MODE, X, 0);
            setValue(EXCLUSIVE_MODE, Y, 0);
            setValue(EXCLUSIVE_MODE, Z, 0);
            setValue(EXCLUSIVE_MODE, T, 0);
            setValue(EXCLUSIVE_MODE, A, 0);
            setValue(EXCLUSIVE_MODE, B, 0);
            setValue(EXCLUSIVE_MODE, C, 0);
            setValue(EXCLUSIVE_MODE, D, 0);
            setValue(EXCLUSIVE_MODE, E, 0);
            setValue(EXCLUSIVE_MODE, F, 0);
            setValue(EXCLUSIVE_MODE, M, 0);
            commit();
        } catch (SQLException e) {
            rollback();
        }
	}
}

	