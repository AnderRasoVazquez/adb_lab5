import java.sql.SQLException;

class ThreadE extends Thread {
    //Attributes
    private String myName = "E";
    private Data myData;

    ThreadE(int mode){
        myData = new Data(mode, mode);
    }

    public void run(){
        try{
            //Concurrent process
            int counter = 0;
            boolean committed;

            myData.synchronize();

            System.out.println("Go " + myName + "!");

            while (counter < Data.NUMBER_OF_ITERATIONS){
                System.out.println("Ejecutando procedimiento " + myName + " por vez " + counter+1);
                committed = myData.procedureE(myName, counter);

                if (committed) {
                    counter += 1;
                }
            }

            myData.finish();
        } catch (SQLException e) {
            System.out.println("Error en la transacción " + myName);
            e.printStackTrace();
        }
    }

}
